package com.college.controllers;

import java.util.List;
import java.util.Optional;
import com.college.dao.CourseDao;
import com.college.domain.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(path="/api/course",produces = "application/json")
public class CourseController {
    @Autowired
    private CourseDao courseDao;

    @GetMapping(value="")
    public List<Course> getCourses() {
        return this.courseDao.findAll();
    }
    @GetMapping(value="/id/{codeCourse}")
    public Optional<Course> getCourses(@PathVariable Long codeCourse) {
        return this.courseDao.findById(codeCourse);
    }
        
    @PostMapping(value="/save")
    public Course saveCourse(@RequestBody Course course){
        return this.courseDao.save(course);
    }

    @PutMapping(value="/update")
    public Course updateCourse(@RequestBody Course course){
        return this.courseDao.saveAndFlush(course);
    }
    @DeleteMapping(value="/id/{codeCourse}")
    public ResponseEntity deleteCourses(@PathVariable Long codeCourse) {
        return this.courseDao.findById(codeCourse).map(course ->{
            this.courseDao.delete(course);
            return ResponseEntity.ok().build();
        })
        .orElseGet(() ->{
            return ResponseEntity.notFound().build();
        });
    }
}
