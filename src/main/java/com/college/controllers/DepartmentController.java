package com.college.controllers;

import java.util.List;
import java.util.Optional;
import com.college.dao.DepartmentDao;
import com.college.domain.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(path="/api/department",produces = "application/json")
public class DepartmentController {
    @Autowired
    private DepartmentDao departmentDao;

    @GetMapping(value="")
    public List<Department> getDepartments() {
        return this.departmentDao.findAll();
    }
    @GetMapping(value="/id/{codeDepartment}")
    public Optional<Department> getDepartments(@PathVariable Long codeDepartment) {
        return this.departmentDao.findById(codeDepartment);
    }
        
    @PostMapping(value="/save")
    public Department saveDepartment(@RequestBody Department department){
        return this.departmentDao.save(department);
    }

    @PutMapping(value="/update")
    public Department updateDepartment(@RequestBody Department department){
        return this.departmentDao.saveAndFlush(department);
    }
    @DeleteMapping(value="/id/{codeDepartment}")
    public ResponseEntity deleteDepartments(@PathVariable Long codeDepartment) {
        return this.departmentDao.findById(codeDepartment).map(department ->{
            this.departmentDao.delete(department);
            return ResponseEntity.ok().build();
        })
        .orElseGet(() ->{
            return ResponseEntity.notFound().build();
        });
    }
}
