package com.college.controllers;
import java.util.List;
import java.util.Optional;
import com.college.dao.CourseDao;
import com.college.dao.EnrolmentDao;
import com.college.dao.StudentDao;
import com.college.domain.Enrolment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(path="/api/enrolment",produces = "application/json")
public class EnrolmentController {
    @Autowired
    private EnrolmentDao enrolmentDao;
    @Autowired
    private StudentDao studentDao;
    @Autowired 
    private CourseDao courseDao;

    @GetMapping(value="")
    public List<Enrolment> getEnrolments() {
        return this.enrolmentDao.findAll();
    }
    @GetMapping(value="/id/{codeEnrolment}")
    public Optional<Enrolment> getEnrolments(@PathVariable Long codeEnrolment) {
        return this.enrolmentDao.findById(codeEnrolment);
    }
       
    @PostMapping(value="/save")
    public Enrolment saveEnrolment(@RequestBody Enrolment enrolment){
        return this.enrolmentDao.save(enrolment);
    }
  
    @PutMapping(value="/update")
    public Enrolment updateEnrolment(@RequestBody Enrolment enrolment){
        return this.enrolmentDao.saveAndFlush(enrolment);
    }
    
    @DeleteMapping(value="/id/{codeEnrolment}")
    public ResponseEntity deleteEnrolments(@PathVariable Long codeEnrolment) {
        return this.enrolmentDao.findById(codeEnrolment).map(enrolment ->{
            this.enrolmentDao.delete(enrolment);
            return ResponseEntity.ok().build();
        })
        .orElseGet(() ->{
            return ResponseEntity.notFound().build();
        });
    }
}
