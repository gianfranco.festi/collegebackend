package com.college.controllers;

import java.util.List;
import java.util.Optional;
import com.college.dao.InstructorDao;
import com.college.domain.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(path="/api/instructor",produces = "application/json")
public class InstructorController {
    @Autowired
    private InstructorDao instructorDao;

    @GetMapping(value="")
    public List<Instructor> getInstructors() {
        return this.instructorDao.findAll();
    }
    @GetMapping(value="/id/{codeInstructor}")
    public Optional<Instructor> getInstructors(@PathVariable Long codeInstructor) {
        return this.instructorDao.findById(codeInstructor);
    }
        
    @PostMapping(value="/save")
    public Instructor saveInstructor(@RequestBody Instructor instructor){
        return this.instructorDao.save(instructor);
    }

    @PutMapping(value="/update")
    public Instructor updateInstructor(@RequestBody Instructor instructor){
        return this.instructorDao.saveAndFlush(instructor);
    }
    @DeleteMapping(value="/id/{codeInstructor}")
    public ResponseEntity deleteInstructors(@PathVariable Long codeInstructor) {
        return this.instructorDao.findById(codeInstructor).map(instructor ->{
            this.instructorDao.delete(instructor);
            return ResponseEntity.ok().build();
        })
        .orElseGet(() ->{
            return ResponseEntity.notFound().build();
        });
    }
}
