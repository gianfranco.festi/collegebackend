package com.college.controllers;

import java.util.List;
import java.util.Optional;
import com.college.dao.StudentDao;
import com.college.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(path="/api/student",produces = "application/json")
public class StudentController {
    @Autowired
    private StudentDao studentDao;

    @GetMapping(value="")
    public List<Student> getStudents() {
        return this.studentDao.findAll();
    }
    @GetMapping(value="/id/{codeStudent}")
    public Optional<Student> getStudents(@PathVariable Long codeStudent) {
        return this.studentDao.findById(codeStudent);
    }
        
    @PostMapping(value="/save")
    public Student saveStudent(@RequestBody Student student){
        return this.studentDao.save(student);
    }

    @PutMapping(value="/update")
    public Student updateStudent(@RequestBody Student student){
        return this.studentDao.saveAndFlush(student);
    }
    @DeleteMapping(value="/id/{codeStudent}")
    public ResponseEntity deleteStudents(@PathVariable Long codeStudent) {
        return this.studentDao.findById(codeStudent).map(student ->{
            this.studentDao.delete(student);
            return ResponseEntity.ok().build();
        })
        .orElseGet(() ->{
            return ResponseEntity.notFound().build();
        })    ;
    }
}
