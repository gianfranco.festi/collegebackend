package com.college.dao;

import com.college.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CourseDao extends JpaRepository<Course,Long>{}