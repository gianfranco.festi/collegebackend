package com.college.dao;
import com.college.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface DepartmentDao extends JpaRepository<Department,Long>{}