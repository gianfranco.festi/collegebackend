package com.college.dao;
import com.college.domain.Enrolment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface EnrolmentDao extends JpaRepository<Enrolment,Long>{}