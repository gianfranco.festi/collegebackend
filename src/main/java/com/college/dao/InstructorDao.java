package com.college.dao;

import com.college.domain.Instructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface InstructorDao  extends JpaRepository<Instructor,Long>{}