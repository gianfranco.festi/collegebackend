package com.college.domain;
import java.sql.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity
@Table(name="courses")
public class Course{
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long codeCourse;
   private Date startDate;
   private Double duration;
   private String description;

   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name="codeDepartment")
   @JsonIdentityReference(alwaysAsId=true) 
   @JsonIdentityInfo(scope = Department.class,generator = ObjectIdGenerators.PropertyGenerator.class, property="codeDepartment")
   private Department department;

   @ManyToOne(fetch =FetchType.EAGER )
   @JoinColumn(name="codeInstructor")
   @JsonIdentityReference(alwaysAsId=true) 
   @JsonIdentityInfo(scope = Instructor.class,generator = ObjectIdGenerators.PropertyGenerator.class, property="codeInstructor")
   private Instructor instructor;

   @OneToMany(mappedBy = "course",cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<Enrolment> enrolments;
}