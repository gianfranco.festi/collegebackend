package com.college.domain;

import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity
@Table(name="departments")
public class Department{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeDepartment;
    private String name;
    private String location;
    private String telephone;
    private Date dateOpened;
    private String description;
    
    @OneToMany(mappedBy = "department",cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Course> courses;
    
}   