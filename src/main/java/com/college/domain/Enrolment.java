package com.college.domain;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
@Table(name="enrolments")
public class Enrolment{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long refNumberEnrolment;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="codeStudent")
    @JsonIdentityReference(alwaysAsId=true) 
    @JsonIdentityInfo(scope=Student.class,generator = ObjectIdGenerators.PropertyGenerator.class, property="codeStudent")
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="codeCourse")
    @JsonIdentityReference(alwaysAsId=true) 
    @JsonIdentityInfo(scope=Course.class,generator = ObjectIdGenerators.PropertyGenerator.class, property="codeCourse")
    private Course course;
}