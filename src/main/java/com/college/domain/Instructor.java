package com.college.domain;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity
@Table(name="instructors")
public class Instructor{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeInstructor;
    private String firstName;
	private String lastName;
	private String address;
	private String homeTel;
	private String workTel;
    private Date dateOfBirth;
    private Date commenced;

    @OneToMany(mappedBy = "instructor",cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Course> courses;
}