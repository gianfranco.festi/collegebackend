package com.college.domain;
import java.sql.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity
@Table(name="students")
public class Student{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeStudent;
    private String firstName;
	private String lastName;
	private String address;
	private String homeTel;
	private String workTel;
    private Date dateOfBirth;

    @OneToMany(mappedBy = "student",cascade = CascadeType.ALL)
    @JsonIgnore 
    private Set<Enrolment> enrolments;
}    

