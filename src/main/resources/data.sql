insert into instructors(code_instructor,first_name,last_name,address,home_tel,work_tel,date_of_birth,commenced )
    values 
		(1,'Tizio','Aldo','Via Roma 10','121212','','1987-12-31','2005-06-25'),
		(2,'Sempronio','Remo','Via Torino 11','15665621212','','1967-12-31','2000-06-25');

insert into students(code_student,first_name,last_name,address,home_tel,work_tel,date_of_birth )
    values 
		(1,'Rossi','Carlo','Via Berlino 11','454545','','2000-12-31'),
		(2,'Verdi','Gianni','Via Milano 1','5321678','','1999-06-15');		

insert into departments(code_department,name,location,telephone,date_opened,description)
	values
		(1,'Mathematics','Building est','65676767','1970-01-01','Department of Mathematics'),
		(2,'ICT','Building North','343434','1980-03-01','Department of ICT');


insert into courses(code_course,start_Date,duration,description,code_instructor,code_department)
	values
	(1,'2019-03-20',100,'ICT software ',1,2),
	(2,'2019-04-2',150,'ICT db',2,2);

insert into enrolments(ref_Number_Enrolment,code_student,code_course)
	values (1,1,1),
				 (2,1,2);
